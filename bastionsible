#!/bin/bash
# https://github.com/epfl-si/ansible.suitcase

set -e
cd "$(cd "$(dirname "$0")"; pwd)"

help () {
    fatal <<HELP_MSG
Usage:

  $0 [ -t sometag ] [ ... ]
HELP_MSG
}

ensure_ansible () {
    if ! test -f ansible-deps-cache/.versions 2>/dev/null; then
        curl https://raw.githubusercontent.com/epfl-si/ansible.suitcase/master/install.sh | \
            SUITCASE_DIR=$PWD/ansible-deps-cache \
            SUITCASE_ANSIBLE_VERSION=7.4.0 \
            bash -x
    fi
    export PATH="$PWD/ansible-deps-cache/bin:$PATH"
    export ANSIBLE_ROLES_PATH="$PWD/ansible-deps-cache/roles"
    export ANSIBLE_COLLECTIONS_PATHS="$PWD/ansible-deps-cache"

    . ansible-deps-cache/lib.sh
}

ensure_ansible

declare -a ansible_args
inventories="test"
while [ "$#" -gt 0 ]; do
  case "$1" in
    --help)
      help ;;
    --all)
      inventories="test-and-prod"
      shift ;;
    --prod)
      inventories="prod"
      shift ;;
    *)
      ansible_args+=("$1")
      shift ;;
  esac
done

inventories() {
  case "$inventories" in
    test)          echo "-i inventory-test.yml" ;;
    prod)          echo "-i inventory-prod.yml" ;;
    test-and-prod) echo "-i inventory-test.yml -i inventory-prod.yml" ;;
  esac
}

set -e -x
ansible-playbook $(inventories) playbook.yml "${ansible_args[@]}"
