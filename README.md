# FSD Bastion

This provide the configuration as code to create a [bastion] to access our
servers whenever we haven't a fixed IP. This means that you will need the [EPFL
VPN] when working remotely, but you will be able to connect to our servers
through this hosts.

For now, it's used as MySQL/MariaDB relay but might be used for [ProxyJump] in
the future.


## Usage

This projet use the [Ansible Suitcase] to configure the remote hosts with
a list of useful software, SSH accesses, customized message of the day,
unattended upgrades configuration and logrotate (todo). It's intended to be a
low-maintenance VM.


### Add yourself to the hosts

The remote hosts are accessed by SSH and keys are added from the [list
of GitHub usernames](./roles/setup/vars/main.yaml). If you want access
to the bastions, please add yourself to the file and open a [merge
request](https://gitlab.epfl.ch/si-idevfsd/bastion/-/merge_requests).


## SSH config

```
##############################
# FSD Bastions
##############################
Host bastion-test itsidevfsd0022 itsidevfsd0022.xaas.epfl.ch
  Hostname 10.95.96.25
  User root
  ForwardAgent yes
  AddKeysToAgent yes

Host bastion-prod itsidevfsd0023 itsidevfsd0023.xaas.epfl.ch
  Hostname 10.95.80.86
  User root
  ForwardAgent yes
  AddKeysToAgent yes
```

## Connect

Once your SSH config is setup, you can create a tunnel with this SSH command:
```
ssh -N -L3307:somedb.epfl.ch:3306 bastion-test
```
Once the tunnel is established, you can connect to a database with:
```
mysql -hlocalhost --port 3307 -usomeuser -p --protocol=tcp
```

_Note: when connecting to localhost, the MySQL client will
try to connect via a Unix socket file by default. The
`--protocol=tcp` argument ensure to use the TCP/IP protocol. See
https://dev.mysql.com/doc/refman/8.0/en/transport-protocols.html for details._


## DBeaver

> DBeaver is a SQL client software application and a database administration
> tool. DBeaver Community Edition is free and open source product. You can
> download it here: https://dbeaver.io/download/

1. Create a new MariaDB connection
   ![doc/DBeaver_new_connection.jpg](doc/DBeaver_new_connection.jpg)

2. Configure the MariaDB properties
   ![doc/DBeaver_connection_settings.jpg](doc/DBeaver_connection_settings.jpg)

3. Add a SSH tunnel though `itsidevfsd0022.xaas.epfl.ch` or
`itsidevfsd0023.xaas.epfl.ch`
   ![doc/DBeaver_SSH_Tunnel.jpg](doc/DBeaver_SSH_Tunnel.jpg)

4. Test the connection
  ![doc/DBeaver_connection_OK.jpg](doc/DBeaver_connection_OK.jpg)

5. It's a good advice to set the connection type (Dev/Test/Prod)
  ![doc/DBeaver_general_settings.jpg](doc/DBeaver_general_settings.jpg)

6. Enjoy and/or repeat for other connection. You can also save the SSH tunnel as
   a profil if you plan to use it for another connection.


## Used by (list of project that use this access)

* [ATARI](https://github.com/epfl-si/atari) (ex-IDP-Exop)



[bastion]: https://en.wikipedia.org/wiki/Bastion_host
[EPFL VPN]: https://www.epfl.ch/campus/services/en/it-services/network-services/remote-intranet-access/vpn-clients-available/
[ProxyJump]: https://wiki.gentoo.org/wiki/SSH_jump_host
[Ansible Suitcase]: https://github.com/epfl-si/ansible.suitcase
